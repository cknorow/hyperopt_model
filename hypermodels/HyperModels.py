from hypermodels.BaseModels import BaseModels

from hyperopt import fmin, tpe, hp, STATUS_OK, Trials

from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from xgboost import XGBClassifier


class HyperModels(BaseModels):
  ''' Implements the specific models in sklearn and ascribes some nice defaults for the 
      hyperopt parameters '''

    
  def model_logistic_regression(self, space=False, **kwargs):
    model_name = self._get_model_name()
    
    default_space = {
                'type':model_name,
                'class_weight':hp.choice('class_weight',[None, 'auto']),
                'C':hp.normal('C',1.0,.3),
                'random_state':hp.choice('random_state',[self.seed]),
                }
    if space:
      return self._get_space(model_name, default_space)
      
    return LogisticRegression(**kwargs)

  def model_gaussianNB(self, space=False, **kwargs):
    model_name = self._get_model_name()

    default_space = { 
                    'type': model_name,
                    'alpha': hp.uniform('alpha', 0.0, 2.0),
                    'random_state':hp.choice('random_state',[self.seed]),
                    } 

    if space:
      return self._get_space(model_name, default_space)

    return GaussianNB(**kwargs)

  def model_svc(self, space=False, **kwargs):
    model_name = self._get_model_name()

    default_space = {
                     'type': model_name,
                     'C': hp.uniform('C', 0, 10.0),
                     'kernel': hp.choice('kernel', ['linear', 'rbf']),
                     'gamma': hp.uniform('gamma', 0, 20.0),
                     'random_state':hp.choice('random_state',[self.seed]),
                    }
    if space:
      return self._get_space(model_name, default_space)

    return LinearSVC(**kwargs)

  def model_random_forest(self, space=False, **kwargs):
    model_name = self._get_model_name()

    default_space = {   
                    'type': model_name,
                    'max_depth': hp.choice('max_depth', range(1,20)),
                    'max_features': hp.choice('max_features', range(1,5)),
                    'n_estimators': hp.choice('n_estimators', range(1,20)),
                    'criterion': hp.choice('criterion', ["gini", "entropy"]),
                    'random_state':hp.choice('random_state',[self.seed]),
                    }

    if space:
      return self._get_space(model_name, default_space)

    return RandomForestClassifier(**kwargs)

  def model_knn(self, space=False, **kwargs):
    model_name = self._get_model_name()

    default_space = {
               'type': model_name,
               'n_neighbors': hp.choice('knn_n_neighbors', range(1,50)),
               'random_state':hp.choice('random_state',[self.seed]),
              }

    if space:
      return self._get_space(model_name, default_space)

    return KNeighborsClassifier(**kwargs)

  def model_xgbc(self, space=False, **kwargs):
    model_name = self._get_model_name()

    default_space = { 
             'type' : model_name,
             'max_depth' : hp.choice('max_depth', range(1,20,2)),
             'n_estimators' : hp.choice('n_estimators', range(20,70,4)),
             'objective' : hp.choice('cirterion',["multi:softprob","binary:logistic"]),
             'colsample_bytree': hp.choice('colsample_bytree',[0.5]),
             'seed' : hp.choice('seed',[self.seed]),
             'subsample' : hp.choice('subsample', [0.5,0.6, 0.7]),
             'learning_rate' : hp.uniform('learning_rate',.2,.4),
              }

    if space:
      return self._get_space(model_name, default_space)

    return XGBClassifier(**kwargs)