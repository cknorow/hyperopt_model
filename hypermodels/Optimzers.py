from sklearn.cross_validation import cross_val_score
from sklearn.cross_validation import KFold

def hyperopt_kfold(hyp, X, y, params, n_folds = 3, random_state=0, score=None):
    t = params['type']
    del params['type']
    clf = hyp.hyperopt_model(t, params)
    return cross_val_score(clf,  X, y, cv=KFold(len(y), n_folds=n_folds, random_state=random_state),  scoring=score).mean(), clf

def hyperopt_cross_val_score(hyp, X, y, params, score=None):
    t = params['type']
    del params['type']
    clf = hyp.hyperopt_model(t, params)
    return cross_val_score(clf,  X, y, scoring=score).mean(), clf