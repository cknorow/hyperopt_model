import inspect

from sklearn.cross_validation import cross_val_score
from sklearn.cross_validation import KFold
from hyperopt import hp


class BaseModels():
  def __init__(self, models=[]):
    self.seed = 0 # its always good to use a set random seed
    
    self.models = models
    self.hyperopt_space()
    

  def hyperopt_space(self):
    ''' updates and returns the hyperopt space '''
    spaces = []
    for model in self.models:
      model_def = '_'.join(['model',model])
      spaces.append(self.get_model_space(model_def))

    self.spaces = hp.choice('classifier_type', spaces)

    return self.spaces

  def update_space(self, model_name, space):
    ''' updates a space for a specific model, then updates
        the global spaces

    \param model_name - the name of the model to update
    \param space - a hyperopt space for the model
    '''
    if model_name in self.models:
      space_name =  '_'.join([model_name, 'space'])
      setattr(self, space_name, space)
      self.hyperopt_space()
    else:
      print 'no model', model_name

  def hyperopt_model(self, model_type, params):
    model_def = '_'.join(['model',model_type])
    return getattr(self, model_def)(**params)

  def get_model_space(self, model_type):
    return getattr(self, model_type)(space=True)

  def list_models(self):
    models = []
    for model in dir(self):
      if model.split('_')[0] == 'model':
        models.append(model.replace('model_',''))
    return models

  def _get_space(self, model_name, space=None):
    """ 
      \param model_name - name of model
      \param space - hyperopt space

      returns the hyperopt model space
      """

    space_name =  '_'.join([model_name, 'space'])
    if not hasattr(self, space_name):
      setattr(self, space_name, space)
    return getattr(self, space_name)

  def _get_model_name(self):
    """" returns the model name defined by a given function """

    return inspect.stack()[1][3].replace('model_','')

