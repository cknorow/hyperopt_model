class ScoreKeeper():
    def __init__(self):
        self.history = []
        self.score = 0
        self.loss = 0
        self.model = None
        self.params = {}
        
    def update_score(self, score, model=None, params=None):
        if self.score < score:
            self.score = score
            self.params = params
            self.model = model
        self.history.append([score,params])
            
    def update_loss(self, loss, params=None):
        if self.loss > loss:
            self.loss = loss
            self.params = params
            self.model = model
        self.history.append([score,params])

