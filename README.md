## hyperot_model 

This program uses hyperopt to optimze hyperparameters for machine learning 
algorithms in sklearn. The goal of this is to provide decent default values 
for the parameter space to search for each algoirthm, while allowing a user
the ability to easily modify the parameter search space when desired.

``` 

from hypermodels.HyperModels import HyperModels
from hyperopt import fmin, tpe, STATUS_OK, Trials

X = ###Training features
y = ###Training targets

def f(params):
    X_ = X[:]
    y_ = y[:]
    print params
    acc = hyp.hyperopt_kfold(X_, y_, params.copy(), score=score)
    print 'loss', acc
    print 'params', params
    return {'loss':-acc, 'status':STATUS_OK}

hyp = hypermodels.hypermodels(models=['logistic_regression', 'random_forest'])
trials = Trials()
best = fmin(f, hyp.spaces, algo=tpe.suggest, max_evals=50, trials=trials)
print 'best:'
print best

```