from hyperopt import fmin, tpe, STATUS_OK, Trials

from hypermodels.HyperModels import HyperModels
import hypermodels.Optimzer as Optimizer

def f(params):
    X_ = X[:]
    y_ = y[:]
    print params
    acc, model = Optimzier.hyperopt_kfold(hyp, X_, y_, params.copy(), score=score)
    skeeper.update_score(acc, model=mode, params=params)
    print 'loss', acc
    print 'params', params
    return {'loss':-acc, 'status':STATUS_OK}

X = ###Training features
y = ###Training targets


skeeper = ScoreKeeper()
hyp = hypermodels.hypermodels(models=['logistic_regression', 'random_forest'])
trials = Trials()
best = fmin(f, hyp.spaces, algo=tpe.suggest, max_evals=50, trials=trials)
print 'best:'
print best