#!/usr/bin/env python
# -*- coding: utf-8 -*-
import setuptools

setuptools.setup(
    name = 'hypermodels',
    version = '0.0.1',
    url = 'http://cdknorow.gitlab.com/hyperopt_models/',
    author = 'Chris Knorowski',
    author_email = 'cknorow@gmail.com',
    description = 'Hyperopt Hyperparameter Optimization for sklearn',
    packages=['hypermodels'],
    classifiers = [
        'Development Status :: 1 - Alpha',
        'Intended Audience :: Education',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Developers',
        'Environment :: Console',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Unix',
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development',
    ],
    platforms = ['Linux'],
    license = 'BSD',
    install_requires = ['numpy', 'scipy', 'hyperopt', 'scikit-learn', 'xgboost'],
)
